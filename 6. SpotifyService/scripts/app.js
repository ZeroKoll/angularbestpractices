angular.module("novatrox.ng", [])
    .service("messagingService", novatrox.messagingService)
    .service("spotifyService", novatrox.spotifyService)
    .directive("novatroxApp", novatrox.appDirective)
    .directive("novatroxDetails", novatrox.detailsDirective);