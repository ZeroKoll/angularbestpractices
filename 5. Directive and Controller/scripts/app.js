angular.module("novatrox.ng", [])
    .service("messagingService", novatrox.messagingService)
    .directive("novatroxApp", novatrox.appDirective)
    .directive("novatroxDetails", novatrox.detailsDirective);