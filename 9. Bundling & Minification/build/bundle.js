System.register("app.config", ["angular"], function(exports_1) {
  var angular;
  var AppConfig;
  return {
    setters: [function(angular_1) {
      angular = angular_1;
    }],
    execute: function() {
      AppConfig = (function() {
        function AppConfig() {}
        AppConfig.config = function(spotifyServiceProvider) {
          var defaults = {delay: 0};
          var config = angular.extend(defaults, window['appConfig'] || {});
          spotifyServiceProvider.setDelay(config.delay);
        };
        return AppConfig;
      })();
      exports_1("AppConfig", AppConfig);
    }
  };
});

System.register("messaging.service", [], function(exports_1) {
  var MessagingService;
  return {
    setters: [],
    execute: function() {
      MessagingService = (function() {
        function MessagingService() {}
        MessagingService.prototype.subscribe = function(topic, callback) {
          return postal.subscribe({
            channel: "demo",
            topic: topic,
            callback: callback
          });
        };
        MessagingService.prototype.publish = function(topic, data) {
          postal.publish({
            channel: "demo",
            topic: topic,
            data: data
          });
        };
        return MessagingService;
      })();
      exports_1("MessagingService", MessagingService);
    }
  };
});

System.register("spotify.service", [], function(exports_1) {
  var SpotifyService,
      SpotifyServiceProvider;
  return {
    setters: [],
    execute: function() {
      SpotifyService = (function() {
        function SpotifyService($http, $q, $timeout, delay) {
          this.$http = $http;
          this.$q = $q;
          this.$timeout = $timeout;
          this.delay = delay;
        }
        SpotifyService.prototype.searchArtists = function(query) {
          var _this = this;
          return this.$q(function(resolve, reject) {
            _this.$timeout(function() {
              _this.$http.get("https://api.spotify.com/v1/search?q=" + encodeURI(query) + "&type=artist").then(function(response) {
                resolve(response.data['artists'].items);
              });
            }, _this.delay);
          });
        };
        return SpotifyService;
      })();
      exports_1("SpotifyService", SpotifyService);
      SpotifyServiceProvider = (function() {
        function SpotifyServiceProvider() {
          this.delay = 0;
          this.setDelay = function(ms) {
            this.delay = ms;
          };
        }
        SpotifyServiceProvider.prototype.$get = function($http, $q, $timeout) {
          return new SpotifyService($http, $q, $timeout, this.delay);
        };
        return SpotifyServiceProvider;
      })();
      exports_1("SpotifyServiceProvider", SpotifyServiceProvider);
    }
  };
});

System.register("app.controller", [], function(exports_1) {
  var AppController;
  return {
    setters: [],
    execute: function() {
      AppController = (function() {
        function AppController(messagingService, spotifyService) {
          this.messagingService = messagingService;
          this.spotifyService = spotifyService;
          this.title = "Spotify";
        }
        AppController.prototype.onQueryChanged = function(query) {
          var _this = this;
          delete this.items;
          if (query.length < 3)
            return;
          this.isLoading = true;
          this.spotifyService.searchArtists(query).then(function(artists) {
            _this.items = artists;
            _this.isLoading = false;
          });
        };
        AppController.prototype.selectItem = function(item) {
          this.messagingService.publish("itemSelected", item);
        };
        return AppController;
      })();
      exports_1("AppController", AppController);
    }
  };
});

System.register("app.directive", ["./app.controller"], function(exports_1) {
  var app_controller_1;
  var AppDirective;
  return {
    setters: [function(app_controller_1_1) {
      app_controller_1 = app_controller_1_1;
    }],
    execute: function() {
      AppDirective = (function() {
        function AppDirective($injector) {
          this.$injector = $injector;
          this.scope = true;
          this.link = function(scope, element, attrs, controller) {
            var ctrl = this.$injector.instantiate(app_controller_1.AppController, {$scope: scope});
            scope[attrs.novatroxApp] = ctrl;
            var watch = scope.$watch(function() {
              return scope[attrs.novatroxApp].query;
            }, function(newVal, oldVal) {
              if (newVal === oldVal)
                return;
              ctrl.onQueryChanged(newVal);
            });
            scope.$on("$destroy", function() {
              watch();
            });
          };
        }
        AppDirective.create = function($injector) {
          return new AppDirective($injector);
        };
        return AppDirective;
      })();
      exports_1("AppDirective", AppDirective);
    }
  };
});

System.register("details.controller", [], function(exports_1) {
  var DetailsController;
  return {
    setters: [],
    execute: function() {
      DetailsController = (function() {
        function DetailsController(messagingService) {
          var _this = this;
          this.hasDetails = true;
          this.subscription = messagingService.subscribe("itemSelected", function(item) {
            _this.hasDetails = true;
            _this.name = item.name;
            if (item.images && item.images.length > 0) {
              _this.imageUrl = item.images[0].url;
            } else {
              _this.imageUrl = null;
            }
          });
        }
        DetailsController.prototype.onUnload = function() {
          this.subscription.unsubscribe();
        };
        return DetailsController;
      })();
      exports_1("DetailsController", DetailsController);
    }
  };
});

System.register("details.directive", ["./details.controller"], function(exports_1) {
  var details_controller_1;
  var DetailsDirective;
  return {
    setters: [function(details_controller_1_1) {
      details_controller_1 = details_controller_1_1;
    }],
    execute: function() {
      DetailsDirective = (function() {
        function DetailsDirective($injector) {
          this.$injector = $injector;
          this.scope = true;
          this.link = function(scope, element, attrs, controller) {
            var ctrl = this.$injector.instantiate(details_controller_1.DetailsController, {$scope: scope});
            scope[attrs.novatroxDetails] = ctrl;
            scope.$on("$destroy", function() {
              ctrl.onUnload();
            });
          };
        }
        DetailsDirective.create = function($injector) {
          return new DetailsDirective($injector);
        };
        return DetailsDirective;
      })();
      exports_1("DetailsDirective", DetailsDirective);
    }
  };
});

System.register("app.js", ["angular", "./app.config", "./messaging.service", "./spotify.service", "./app.directive", "./details.directive"], function(exports_1) {
  var angular,
      app_config_1,
      messaging_service_1,
      spotify_service_1,
      app_directive_1,
      details_directive_1;
  return {
    setters: [function(angular_1) {
      angular = angular_1;
    }, function(app_config_1_1) {
      app_config_1 = app_config_1_1;
    }, function(messaging_service_1_1) {
      messaging_service_1 = messaging_service_1_1;
    }, function(spotify_service_1_1) {
      spotify_service_1 = spotify_service_1_1;
    }, function(app_directive_1_1) {
      app_directive_1 = app_directive_1_1;
    }, function(details_directive_1_1) {
      details_directive_1 = details_directive_1_1;
    }],
    execute: function() {
      angular.module("novatrox.ng", []).service("messagingService", messaging_service_1.MessagingService).provider("spotifyService", spotify_service_1.SpotifyServiceProvider).directive("novatroxApp", app_directive_1.AppDirective.create).directive("novatroxDetails", details_directive_1.DetailsDirective.create).config(app_config_1.AppConfig.config);
      angular.element(document).ready(function() {
        angular.bootstrap(document, ['novatrox.ng']);
      });
    }
  };
});
