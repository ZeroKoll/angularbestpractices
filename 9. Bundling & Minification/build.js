var path = require("path");
var Builder = require('systemjs-builder');

var builder = new Builder('scripts', 'build.config.js');

builder
.bundle('app.js', 'build/bundle.js', {
            fetch: function (load, fetch) {
                if (load.name.substr(load.name.length - 3) !== '.js') {
                    load.name += '.js';
                    load.address += '.js';
                }
                return fetch(load);
            },
            minify: false
        })
.then(function() {
  console.log('Build complete');
})
.catch(function(err) {
  console.log('Build error');
  console.log(err);
});