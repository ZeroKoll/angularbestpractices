System.register([], function(exports_1) {
    var AppController;
    return {
        setters:[],
        execute: function() {
            AppController = (function () {
                function AppController(messagingService, spotifyService) {
                    this.messagingService = messagingService;
                    this.spotifyService = spotifyService;
                    this.title = "Spotify";
                }
                AppController.prototype.onQueryChanged = function (query) {
                    var _this = this;
                    delete this.items;
                    if (query.length < 3)
                        return;
                    this.isLoading = true;
                    this.spotifyService.searchArtists(query)
                        .then(function (artists) {
                        _this.items = artists;
                        _this.isLoading = false;
                    });
                };
                AppController.prototype.selectItem = function (item) {
                    this.messagingService.publish("itemSelected", item);
                };
                return AppController;
            })();
            exports_1("AppController", AppController);
        }
    }
});
//# sourceMappingURL=app.controller.js.map