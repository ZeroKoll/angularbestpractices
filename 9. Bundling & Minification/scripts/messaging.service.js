System.register([], function(exports_1) {
    var MessagingService;
    return {
        setters:[],
        execute: function() {
            MessagingService = (function () {
                function MessagingService() {
                }
                MessagingService.prototype.subscribe = function (topic, callback) {
                    return postal.subscribe({
                        channel: "demo",
                        topic: topic,
                        callback: callback
                    });
                };
                MessagingService.prototype.publish = function (topic, data) {
                    postal.publish({
                        channel: "demo",
                        topic: topic,
                        data: data
                    });
                };
                return MessagingService;
            })();
            exports_1("MessagingService", MessagingService);
        }
    }
});
//# sourceMappingURL=messaging.service.js.map