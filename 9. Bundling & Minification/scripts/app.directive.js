/// <reference path="../../typings/angularjs/angular.d.ts" />
System.register(['./app.controller'], function(exports_1) {
    var app_controller_1;
    var AppDirective;
    return {
        setters:[
            function (app_controller_1_1) {
                app_controller_1 = app_controller_1_1;
            }],
        execute: function() {
            AppDirective = (function () {
                function AppDirective($injector) {
                    this.$injector = $injector;
                    this.scope = true;
                    this.link = function (scope, element, attrs, controller) {
                        var ctrl = this.$injector.instantiate(app_controller_1.AppController, { $scope: scope });
                        scope[attrs.novatroxApp] = ctrl;
                        var watch = scope.$watch(function () { return scope[attrs.novatroxApp].query; }, function (newVal, oldVal) {
                            if (newVal === oldVal)
                                return;
                            ctrl.onQueryChanged(newVal);
                        });
                        scope.$on("$destroy", function () {
                            watch();
                        });
                    };
                }
                AppDirective.create = function ($injector) {
                    return new AppDirective($injector);
                };
                return AppDirective;
            })();
            exports_1("AppDirective", AppDirective);
        }
    }
});
//# sourceMappingURL=app.directive.js.map