/// <reference path="../../typings/angularjs/angular.d.ts" />
System.register([], function(exports_1) {
    var AppConfig;
    return {
        setters:[],
        execute: function() {
            AppConfig = (function () {
                function AppConfig() {
                }
                AppConfig.config = function (spotifyServiceProvider) {
                    var defaults = {
                        delay: 0
                    };
                    var config = angular.extend(defaults, window['appConfig'] || {});
                    spotifyServiceProvider.setDelay(config.delay);
                };
                return AppConfig;
            })();
            exports_1("AppConfig", AppConfig);
        }
    }
});
//# sourceMappingURL=app.config.js.map