/// <reference path="../../typings/angularjs/angular.d.ts" />
System.register(['./app.config', './messaging.service', './spotify.service', './app.directive', './details.directive'], function(exports_1) {
    var app_config_1, messaging_service_1, spotify_service_1, app_directive_1, details_directive_1;
    return {
        setters:[
            function (app_config_1_1) {
                app_config_1 = app_config_1_1;
            },
            function (messaging_service_1_1) {
                messaging_service_1 = messaging_service_1_1;
            },
            function (spotify_service_1_1) {
                spotify_service_1 = spotify_service_1_1;
            },
            function (app_directive_1_1) {
                app_directive_1 = app_directive_1_1;
            },
            function (details_directive_1_1) {
                details_directive_1 = details_directive_1_1;
            }],
        execute: function() {
            angular.module("novatrox.ng", [])
                .service("messagingService", messaging_service_1.MessagingService)
                .provider("spotifyService", spotify_service_1.SpotifyServiceProvider)
                .directive("novatroxApp", app_directive_1.AppDirective.create)
                .directive("novatroxDetails", details_directive_1.DetailsDirective.create)
                .config(app_config_1.AppConfig.config);
            angular.element(document).ready(function () {
                angular.bootstrap(document, ['novatrox.ng']);
            });
        }
    }
});
//# sourceMappingURL=app.js.map