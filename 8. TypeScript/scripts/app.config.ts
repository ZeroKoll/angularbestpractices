/// <reference path="../../typings/angularjs/angular.d.ts" />

export class AppConfig {
    public static config(spotifyServiceProvider) {
        var defaults = {
            delay: 0
        }

        var config = angular.extend(defaults, window['appConfig'] || {});
        
        spotifyServiceProvider.setDelay(config.delay);
    }
}