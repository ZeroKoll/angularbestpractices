System.register([], function(exports_1) {
    var DetailsController;
    return {
        setters:[],
        execute: function() {
            DetailsController = (function () {
                function DetailsController(messagingService) {
                    var _this = this;
                    this.hasDetails = true;
                    this.subscription = messagingService.subscribe("itemSelected", function (item) {
                        _this.hasDetails = true;
                        _this.name = item.name;
                        if (item.images && item.images.length > 0) {
                            _this.imageUrl = item.images[0].url;
                        }
                        else {
                            _this.imageUrl = null;
                        }
                    });
                }
                DetailsController.prototype.onUnload = function () {
                    this.subscription.unsubscribe();
                };
                return DetailsController;
            })();
            exports_1("DetailsController", DetailsController);
        }
    }
});
//# sourceMappingURL=details.controller.js.map