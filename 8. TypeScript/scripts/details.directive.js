/// <reference path="../../typings/angularjs/angular.d.ts" />
System.register(['./details.controller'], function(exports_1) {
    var details_controller_1;
    var DetailsDirective;
    return {
        setters:[
            function (details_controller_1_1) {
                details_controller_1 = details_controller_1_1;
            }],
        execute: function() {
            DetailsDirective = (function () {
                function DetailsDirective($injector) {
                    this.$injector = $injector;
                    this.scope = true;
                    this.link = function (scope, element, attrs, controller) {
                        var ctrl = this.$injector.instantiate(details_controller_1.DetailsController, { $scope: scope });
                        scope[attrs.novatroxDetails] = ctrl;
                        scope.$on("$destroy", function () {
                            ctrl.onUnload();
                        });
                    };
                }
                DetailsDirective.create = function ($injector) {
                    return new DetailsDirective($injector);
                };
                return DetailsDirective;
            })();
            exports_1("DetailsDirective", DetailsDirective);
        }
    }
});
//# sourceMappingURL=details.directive.js.map