angular.module("novatrox.ng", [])
    .controller("appCtrl", novatrox.appController)
    .controller("detailsCtrl", novatrox.detailsController);