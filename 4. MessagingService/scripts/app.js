angular.module("novatrox.ng", [])
    .service("messagingService", novatrox.messagingService)
    .controller("appCtrl", novatrox.appController)
    .controller("detailsCtrl", novatrox.detailsController);