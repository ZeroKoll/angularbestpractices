angular.module("novatrox.ng", [])
    .service("messagingService", novatrox.messagingService)
    .provider("spotifyService", novatrox.spotifyServiceProvider)
    .directive("novatroxApp", novatrox.appDirective)
    .directive("novatroxDetails", novatrox.detailsDirective)
    .config(novatrox.appConfig);